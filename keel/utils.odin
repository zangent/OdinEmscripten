// Generic utilities.
// If this gets too bloated, separate these out into various files.

#import "os.odin";
#import "fmt.odin";
#import "mem.odin";
#import "strings.odin";

// TODO(zachary): This string shit is getting ridiculous.

read_file_to_string :: proc(path: string) -> (string, bool) #inline {

	cstr, success := os.read_entire_file(path);

	if(!success) {
		return "", false;
	}
	return strings.to_odin_string(^cstr[0]), true;
}

// Concatenate
// NOTE(zachary): YOU HAVE TO FREE THIS!
cat :: proc(strs: ..string) -> ([]byte, string) {
	cap, current: int = 0, 0;
	for s in strs {
		cap += len(s);
	}
	buf := make([]byte, cap+1);
	for s in strs {
		for i:=0; i<len(s); i+=1 {
			#no_bounds_check
			buf[current] = s[i];
			current+=1;
		}
	}
	buf[len(buf)-1] = 0;
	return buf, strings.to_odin_string(^buf[0]);
}