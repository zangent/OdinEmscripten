
// These are just wrappers made to enforce types, since these can easily be mixed up.
// NOTE(zachary): I only feel good about doing this because these are used so infrequently.

Window :: struct {
	data: ^u8
};
GlContext :: struct {
	data: ^u8
};


// Real structs.

CommonEvent :: struct #ordered {
	type_id, timestamp: u32
};

Keysym :: struct #ordered {
	scancode, sym: i32,
	mod: u16,
	unused: u32,
}