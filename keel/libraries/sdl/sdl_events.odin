#load "sdl_types.odin";

// TODO(zachary): Add all of the event types.
//   For now, I'm only adding what Keel _needs_.
Event :: raw_union {
	type_id   : u32,                          // Event type, shared with all events
	common    : CommonEvent,                  // Common event data
//	window    : WindowEvent,                  // Window event data
	key       : KeyboardEvent,                // Keyboard event data
//	edit      : TextEditingEvent,             // Text editing event data
//	text      : TextInputEvent,               // Text input event data
	motion    : MouseMotionEvent,             // Mouse motion event data
	button    : MouseButtonEvent,             // Mouse button event data
	wheel     : MouseWheelEvent,              // Mouse wheel event data
//	jaxis     : JoyAxisEvent,                 // Joystick axis event data
//	jball     : JoyBallEvent,                 // Joystick ball event data
//	jhat      : JoyHatEvent,                  // Joystick hat event data
//	jbutton   : JoyButtonEvent,               // Joystick button event data
//	jdevice   : JoyDeviceEvent,               // Joystick device change event data
//	caxis     : ControllerAxisEvent,          // Game Controller axis event data
//	cbutton   : ControllerButtonEvent,        // Game Controller button event data
//	cdevice   : ControllerDeviceEvent,        // Game Controller device event data
//	adevice   : AudioDeviceEvent,             // Audio device event data
//	quit      : QuitEvent,                    // Quit request event data
//	user      : UserEvent,                    // Custom event data
//	syswm     : SysWMEvent,                   // System dependent window event data
//	tfinger   : TouchFingerEvent,             // Touch finger event data
//	mgesture  : MultiGestureEvent,            // Gesture event data
//	dgesture  : DollarGestureEvent,           // Gesture event data
//	drop      : DropEvent,                    // Drag and drop event data

	padding : [56] u8
};

KeyboardEvent :: struct #ordered {
	type, timestamp, window_id: u32,
	state, repeat, pad2, pad3: u8,
	keysym: Keysym
}

MouseMotionEvent :: struct #ordered {
	type, timestamp, window_id, which, state: u32,
	x, y, motion_x, motion_y: i32
}

MouseButtonEvent :: struct #ordered {
	type, timestamp, window_id, which: u32,
	button, state, clicks, pad1: u8,
	x, y: i32
}

MouseWheelEvent :: struct #ordered {
	type, timestamp, window_id, which: u32,
	x, y: i32,
	direction: u32
}
