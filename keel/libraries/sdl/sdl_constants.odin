

INIT_TIMER          : u32 : 0x00000001;
INIT_AUDIO          : u32 : 0x00000010;
INIT_VIDEO          : u32 : 0x00000020;  // SDL_INIT_VIDEO implies SDL_INIT_EVENTS
INIT_JOYSTICK       : u32 : 0x00000200;  // SDL_INIT_JOYSTICK implies SDL_INIT_EVENTS
INIT_HAPTIC         : u32 : 0x00001000;
INIT_GAMECONTROLLER : u32 : 0x00002000;  // SDL_INIT_GAMECONTROLLER implies SDL_INIT_JOYSTICK
INIT_EVENTS         : u32 : 0x00004000;
INIT_EVERYTHING     : u32 : INIT_TIMER | INIT_AUDIO | INIT_VIDEO | INIT_EVENTS | INIT_JOYSTICK | INIT_HAPTIC | INIT_GAMECONTROLLER;


WINDOWPOS_CENTERED  : int : 0x2FFF0000;
WINDOWPOS_UNDEFINED : int : 0x1FFF0000;

WINDOW_FULLSCREEN          :: 0x00000001;     // fullscreen window
WINDOW_OPENGL              :: 0x00000002;     // window usable with OpenGL context
WINDOW_SHOWN               :: 0x00000004;     // window is visible
WINDOW_HIDDEN              :: 0x00000008;     // window is not visible
WINDOW_BORDERLESS          :: 0x00000010;     // no window decoration
WINDOW_RESIZABLE           :: 0x00000020;     // window can be resized
WINDOW_MINIMIZED           :: 0x00000040;     // window is minimized
WINDOW_MAXIMIZED           :: 0x00000080;     // window is maximized
WINDOW_INPUT_GRABBED       :: 0x00000100;     // window has grabbed input focus
WINDOW_INPUT_FOCUS         :: 0x00000200;     // window has input focus
WINDOW_MOUSE_FOCUS         :: 0x00000400;     // window has mouse focus
WINDOW_FULLSCREEN_DESKTOP  :: ( WINDOW_FULLSCREEN | 0x00001000 );
WINDOW_FOREIGN             :: 0x00000800;     // window not created by SDL
WINDOW_ALLOW_HIGHDPI       :: 0x00002000;     // window should be created in high-DPI mode if supported
WINDOW_MOUSE_CAPTURE       :: 0x00004000;     // window has mouse captured (unrelated to INPUT_GRABBED)
WINDOW_ALWAYS_ON_TOP       :: 0x00008000;     // window should always be above others
WINDOW_SKIP_TASKBAR        :: 0x00010000;     // window should not be added to the taskbar
WINDOW_UTILITY             :: 0x00020000;     // window should be treated as a utility window
WINDOW_TOOLTIP             :: 0x00040000;     // window should be treated as a tooltip
WINDOW_POPUP_MENU          :: 0x00080000;     // window should be treated as a popup menu

MOUSE_BUTTON_LEFT          :: 1;
MOUSE_BUTTON_MIDDLE        :: 2;
MOUSE_BUTTON_RIGHT         :: 3;
MOUSE_BUTTON_X1            :: 4;
MOUSE_BUTTON_X2            :: 5;
MOUSE_BUTTON_LMASK         :: (1 << (MOUSE_BUTTON_LEFT - 1));
MOUSE_BUTTON_MMASK         :: (1 << (MOUSE_BUTTON_MIDDLE - 1));
MOUSE_BUTTON_RMASK         :: (1 << (MOUSE_BUTTON_RIGHT - 1));
MOUSE_BUTTON_X1MASK        :: (1 << (MOUSE_BUTTON_X1 - 1));
MOUSE_BUTTON_X2MASK        :: (1 << (MOUSE_BUTTON_X2 - 1));

GlAttr :: enum {
	RED_SIZE,
	GREEN_SIZE,
	BLUE_SIZE,
	ALPHA_SIZE,
	BUFFER_SIZE,
	DOUBLEBUFFER,
	DEPTH_SIZE,
	STENCIL_SIZE,
	ACCUM_RED_SIZE,
	ACCUM_GREEN_SIZE,
	ACCUM_BLUE_SIZE,
	ACCUM_ALPHA_SIZE,
	STEREO,
	MULTISAMPLEBUFFERS,
	MULTISAMPLESAMPLES,
	ACCELERATED_VISUAL,
	RETAINED_BACKING,
	CONTEXT_MAJOR_VERSION,
	CONTEXT_MINOR_VERSION,
	CONTEXT_EGL,
	CONTEXT_FLAGS,
	CONTEXT_PROFILE_MASK,
	SHARE_WITH_CURRENT_CONTEXT,
	FRAMEBUFFER_SRGB_CAPABLE,
	CONTEXT_RELEASE_BEHAVIOR
};

GlProfile :: enum {
	CONTEXT_PROFILE_CORE          = 0x0001,
	CONTEXT_PROFILE_COMPATIBILITY = 0x0002,
	CONTEXT_PROFILE_ES            = 0x0004, // GLX_CONTEXT_ES2_PROFILE_BIT_EXT
};

SystemCursor :: enum {
	ARROW,     // Arrow
	IBEAM,     // I-beam
	WAIT,      // Wait
	CROSSHAIR, // Crosshair
	WAITARROW, // Small wait cursor (or Wait if not available)
	SIZENWSE,  // Double arrow pointing northwest and southeast
	SIZENESW,  // Double arrow pointing northeast and southwest
	SIZEWE,    // Double arrow pointing west and east
	SIZENS,    // Double arrow pointing north and south
	SIZEALL,   // Four pointed arrow pointing north, south, east, and west
	NO,        // Slashed circle or crossbones
	HAND,      // Hand
};

MOUSEWHEEL_NORMAL  :: 0;
MOUSEWHEEL_FLIPPED :: 1;

SdlBool :: enum {
	FALSE = 0,
	TRUE = 1
}

EventType :: enum {

	FIRSTEVENT     = 0,     // Unused (do not remove)

	// Application events
	QUIT           = 0x100, // User-requested quit

	// These application events have special meaning on iOS, see README-ios.md for details
	APP_TERMINATING,        /* The application is being terminated by the OS
	                                 Called on iOS in applicationWillTerminate()
	                                 Called on Android in onDestroy()
	                            */
	APP_LOWMEMORY,          /* The application is low on memory, free memory if possible.
	                                 Called on iOS in applicationDidReceiveMemoryWarning()
	                                 Called on Android in onLowMemory()
	                            */
	APP_WILLENTERBACKGROUND, /* The application is about to enter the background
	                                 Called on iOS in applicationWillResignActive()
	                                 Called on Android in onPause()
	                            */
	APP_DIDENTERBACKGROUND, /* The application did enter the background and may not get CPU for some time
	                                 Called on iOS in applicationDidEnterBackground()
	                                 Called on Android in onPause()
	                            */
	APP_WILLENTERFOREGROUND, /* The application is about to enter the foreground
	                                 Called on iOS in applicationWillEnterForeground()
	                                 Called on Android in onResume()
	                            */
	APP_DIDENTERFOREGROUND, /* The application is now interactive
	                                 Called on iOS in applicationDidBecomeActive()
	                                 Called on Android in onResume()
	                            */

	// Window events
	WINDOWEVENT    = 0x200, // Window state change
	SYSWMEVENT,             // System specific event

	// Keyboard events
	KEYDOWN        = 0x300, // Key pressed
	KEYUP,                  // Key released
	TEXTEDITING,            // Keyboard text editing (composition)
	TEXTINPUT,              // Keyboard text input
	KEYMAPCHANGED,          /* Keymap changed due to a system event such as an
	                                 input language or keyboard layout change.
	                            */

	// Mouse events
	MOUSEMOTION    = 0x400, // Mouse moved
	MOUSEBUTTONDOWN,        // Mouse button pressed
	MOUSEBUTTONUP,          // Mouse button released
	MOUSEWHEEL,             // Mouse wheel motion

	// Joystick events
	JOYAXISMOTION  = 0x600, // Joystick axis motion
	JOYBALLMOTION,          // Joystick trackball motion
	JOYHATMOTION,           // Joystick hat position change
	JOYBUTTONDOWN,          // Joystick button pressed
	JOYBUTTONUP,            // Joystick button released
	JOYDEVICEADDED,         // A new joystick has been inserted into the system
	JOYDEVICEREMOVED,       // An opened joystick has been removed

	// Game controller events
	CONTROLLERAXISMOTION  = 0x650, // Game controller axis motion
	CONTROLLERBUTTONDOWN,          // Game controller button pressed
	CONTROLLERBUTTONUP,            // Game controller button released
	CONTROLLERDEVICEADDED,         // A new Game controller has been inserted into the system
	CONTROLLERDEVICEREMOVED,       // An opened Game controller has been removed
	CONTROLLERDEVICEREMAPPED,      // The controller mapping was updated

	// Touch events
	FINGERDOWN      = 0x700,
	FINGERUP,
	FINGERMOTION,

	// Gesture events
	DOLLARGESTURE   = 0x800,
	DOLLARRECORD,
	MULTIGESTURE,

	// Clipboard events
	CLIPBOARDUPDATE = 0x900, // The clipboard changed

	// Drag and drop events
	DROPFILE        = 0x1000, // The system requests a file open
	DROPTEXT,                 // text/plain drag-and-drop event
	DROPBEGIN,                // A new set of drops is beginning (NULL filename)
	DROPCOMPLETE,             // Current set of drops is now complete (NULL filename)

	// Audio hotplug events
	AUDIODEVICEADDED = 0x1100, // A new audio device is available
	AUDIODEVICEREMOVED,        // An audio device has been removed.

	// Render events
	RENDER_TARGETS_RESET = 0x2000, // The render targets have been reset and their contents need to be updated
	RENDER_DEVICE_RESET, // The device has been reset and all textures need to be recreated

	/** Events ::USEREVENT through ::LASTEVENT are for your use,
	 *  and should be allocated with RegisterEvents()
	 */
	USEREVENT    = 0x8000,

	/**
	 *  This last event is only for bounding internal arrays
	 */
	LASTEVENT    = 0xFFFF
};