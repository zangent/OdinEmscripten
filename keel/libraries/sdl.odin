// SDL bindings for Odin
// Most of this is copy-pasted from various SDL headers,
//   then picked apart to be valid Odin code.

// NOTE: These bindings are not and will not be complete.
//       I'm only going to include what I use.

// If you want to get to the API, just ctrl-f for "Friendly functions"


#foreign_system_library sdl "win32libs/SDL2.lib" when (ODIN_OS == "windows");
#foreign_system_library sdl "SDL2"               when (ODIN_OS == "osx" || ODIN_OS == "linux");

#load "sdl/sdl_types.odin";
#load "sdl/sdl_constants.odin";
#load "sdl/sdl_scancode.odin";
#load "sdl/sdl_keycode.odin";
#load "sdl/sdl_events.odin";

#import "../utils.odin";
#import "strings.odin";


// Annoying functions

// NOTE(zachary): These functions take types (like c strings) that are annoying to get to in Odin.
//   Down in the friendly functions section we'll have wrappers for these.

_create_window           :: proc(title: ^u8, x, y, w, h: int, flags: u32) -> ^u8                   #foreign sdl "SDL_CreateWindow";
_gl_create_context       :: proc(window: ^u8) -> ^u8                                               #foreign sdl "SDL_GL_CreateContext";
_gl_delete_context       :: proc(gl_context: ^u8)                                                  #foreign sdl "SDL_GL_DeleteContext";
_gl_make_current         :: proc(window, gl_context: ^u8) -> int                                   #foreign sdl "SDL_GL_MakeCurrent";
_gl_swap_window          :: proc(window: ^u8)                                                      #foreign sdl "SDL_GL_SwapWindow";
_load_library            :: proc(lib_path: ^u8) -> rawptr                                          #foreign sdl "SDL_LoadObject";
_load_function           :: proc(handle: rawptr, name: ^u8) -> rawptr                              #foreign sdl "SDL_LoadFunction";
_get_scancode_name       :: proc(scancode: i32) -> ^u8                                             #foreign sdl "SDL_GetScancodeName";
_get_scancode_from_name  :: proc(name: ^u8) -> i32                                                 #foreign sdl "SDL_GetScancodeFromName";
_get_key_name            :: proc(keycode: i32) -> ^u8                                              #foreign sdl "SDL_GetKeyName";
_get_key_from_name       :: proc(name: ^u8) -> i32                                                 #foreign sdl "SDL_GetKeyFromName";
_get_mouse_state         :: proc(x, y: ^int) -> u32                                                #foreign sdl "SDL_GetMouseState";
_set_relative_mouse_mode :: proc(val: SdlBool) -> int                                              #foreign sdl "SDL_SetRelativeMouseMode";

// Friendly functions


init                 :: proc(flags: u32) -> int                                                    #foreign sdl "SDL_Init";
sleep                :: proc(ms: u32)                                                              #foreign sdl "SDL_Delay";
get_ticks            :: proc() -> u32                                                              #foreign sdl "SDL_GetTicks";
poll_event           :: proc(event_ptr: ^Event) -> int                                             #foreign sdl "SDL_PollEvent";
gl_set_attribute     :: proc(flags: GlAttr, value: int) -> int                                     #foreign sdl "SDL_GL_SetAttribute";
move_mouse_in_window :: proc(window: ^Window, x, y: int)                                           #foreign sdl "SDL_WarpMouseInWindow";
create_window        :: proc(title: string, x, y, w, h: int, flags: u32) -> (Window, bool) #inline {

	title_c_str := strings.new_c_string(title);
	ptr: ^u8 = _create_window(title_c_str, x, y, w, h, flags);
	free(title_c_str);

	if(ptr == nil) {
		return Window{ptr}, true;
	}

	return Window{ptr}, false;
}
gl_create_context :: proc(window: Window) -> (GlContext, bool) #inline {

	ptr: ^u8 = _gl_create_context(window.data);
	if(ptr == nil) {
		return GlContext{ptr}, true;
	}

	return GlContext{ptr}, false;
}
gl_delete_context :: proc(gl_context: GlContext) #inline {
	_gl_delete_context(gl_context.data);
}
make_context_current :: proc(window: Window, gl_context: GlContext) -> int #inline {
	return _gl_make_current(window.data, gl_context.data);
}
gl_swap_window :: proc(window: Window) #inline {
	_gl_swap_window(window.data);
}
load_library :: proc(lib_path: string) -> rawptr #inline {
	lib_c_str := strings.new_c_string(lib_path);
	lib_handle := _load_library(lib_c_str);
	free(lib_c_str);
	return lib_handle;
}
load_function :: proc(handle: rawptr, name: string) -> rawptr #inline {
	name_c_str := strings.new_c_string(name);
	proc_handle := _load_function(handle, name_c_str);
	free(name_c_str);
	return proc_handle;
}
fast_scancode_to_keycode :: proc(c: i32) -> i32 #inline {return c | (1 << 30);}
scancode_to_keycode :: proc(s: i32) -> i32 #foreign sdl "SDL_GetKeyFromScancode";
keycode_to_scancode :: proc(c: i32) -> i32 #foreign sdl "SDL_GetScancodeFromKey";
get_scancode_name :: proc(scancode: i32) -> string #inline {
	return strings.to_odin_string(_get_scancode_name(scancode));
}
get_scancode_from_name :: proc(name: string) -> i32 #inline {
	name_c_str := strings.new_c_string(name);
	scancode := _get_scancode_from_name(name_c_str);
	free(name_c_str);
	return scancode;
}
get_key_name :: proc(keycode: i32) -> string #inline {
	return strings.to_odin_string(_get_key_name(keycode));
}
get_key_from_name :: proc(name: string) -> i32 #inline {
	name_c_str := strings.new_c_string(name);
	key := _get_key_from_name(name_c_str);
	free(name_c_str);
	return key;
}
get_mouse_state :: proc() -> (int, int, u32) #inline {
	x, y: int;
	retval := _get_mouse_state(^x, ^y);
	return x, y, retval;
}
set_relative_mouse_mode :: proc(mode: bool) -> bool #inline {
	if(mode) {
		return _set_relative_mouse_mode(SdlBool.TRUE) != 0;
	}
	return _set_relative_mouse_mode(SdlBool.FALSE) != 0;
}