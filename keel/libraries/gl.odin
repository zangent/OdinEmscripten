
// TODO(zachary): When Bill implements compile-time function execution, make this use
//   that instead of having to duplicate function names. This MacOS stuff is getting ridiculous.

#load "opengl_constants.odin";

#foreign_system_library ogl "opengl32.lib"            when ODIN_OS == "windows";
#foreign_system_library ogl "GL"                      when ODIN_OS == "linux";
#foreign_system_library ogl "-fOpenGL"                when ODIN_OS == "osx";

#load "gl/gl_dynamic.odin"  when (ODIN_OS == "windows" || ODIN_OS == "linux");
#load "gl/gl_macos.odin"    when (ODIN_OS == "osx");

// Functions that are _not_ loaded dynamically.

clear           :: proc(mask: u32)                                                                 #foreign ogl "glClear";
clear_color     :: proc(r, g, b, a: f32)                                                           #foreign ogl "glClearColor";
begin           :: proc(mode: i32)                                                                 #foreign ogl "glBegin";
end             :: proc()                                                                          #foreign ogl "glEnd";
finish          :: proc()                                                                          #foreign ogl "glFinish";
blend_func      :: proc(sfactor, dfactor: i32)                                                     #foreign ogl "glBlendFunc";
depth_func      :: proc(func: u32)                                                                 #foreign ogl "glDepthFunc";
enable          :: proc(cap: i32)                                                                  #foreign ogl "glEnable";
disable         :: proc(cap: i32)                                                                  #foreign ogl "glDisable";
gen_textures    :: proc(count: i32, result: ^u32)                                                  #foreign ogl "glGenTextures";
delete_textures :: proc(count: i32, result: ^u32)                                                  #foreign ogl "glDeleteTextures";
tex_parameteri  :: proc(target, pname, param: i32)                                                 #foreign ogl "glTexParameteri";
tex_parameterf  :: proc(target: i32, pname: i32, param: f32)                                       #foreign ogl "glTexParameterf";
bind_texture    :: proc(target: i32, texture: u32)                                                 #foreign ogl "glBindTexture";
load_identity   :: proc()                                                                          #foreign ogl "glLoadIdentity";
viewport        :: proc(x, y, width, height: i32)                                                  #foreign ogl "glViewport";
ortho           :: proc(left, right, bottom, top, near, far: f64)                                  #foreign ogl "glOrtho";
color3f         :: proc(r, g, b: f32)                                                              #foreign ogl "glColor3f";
vertex3f        :: proc(x, y, z: f32)                                                              #foreign ogl "glVertex3f";
tex_image2d     :: proc(target, level, internal_format, width, height,
                        border, format, _type: i32, pixels: rawptr)                                #foreign ogl "glTexImage2D";

get_error       :: proc() -> i32                                                                   #foreign ogl "glGetError";
get_string      :: proc(name: i32) -> ^byte                                                        #foreign ogl "glGetString";
get_integerv    :: proc(name: i32, v: ^i32)                                                        #foreign ogl "glGetIntegerv";

/*
GL_FUNCTION :: struct {
	name, args : string
};

get_gl_funcs :: proc() -> []GL_FUNCTION {

	gl_functions := [] GL_FUNCTION {
		GL_FUNCTION{"glEnable", "flag: i32"},
	};
	return gl_functions;
}
*/
